﻿
namespace OAMS_Interface_VFS
{
    partial class addResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_back = new System.Windows.Forms.Button();
            this.tb_SID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tb_result = new System.Windows.Forms.TextBox();
            this.tb_unit = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_addResult = new System.Windows.Forms.Button();
            this.tb_TOID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_testName = new System.Windows.Forms.TextBox();
            this.tb_testID = new System.Windows.Forms.TextBox();
            this.cb_testList = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgv_results = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_results)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_back
            // 
            this.btn_back.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_back.BackColor = System.Drawing.Color.Navy;
            this.btn_back.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btn_back.FlatAppearance.BorderSize = 3;
            this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_back.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_back.ForeColor = System.Drawing.Color.White;
            this.btn_back.Location = new System.Drawing.Point(611, 30);
            this.btn_back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(147, 34);
            this.btn_back.TabIndex = 7;
            this.btn_back.Text = "Finish";
            this.btn_back.UseVisualStyleBackColor = false;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // tb_SID
            // 
            this.tb_SID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tb_SID.Enabled = false;
            this.tb_SID.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_SID.Location = new System.Drawing.Point(136, 23);
            this.tb_SID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_SID.Name = "tb_SID";
            this.tb_SID.Size = new System.Drawing.Size(213, 34);
            this.tb_SID.TabIndex = 9;
            this.tb_SID.TextChanged += new System.EventHandler(this.tb_TID_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Specimen ID";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tb_result);
            this.groupBox1.Controls.Add(this.tb_unit);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.btn_addResult);
            this.groupBox1.Controls.Add(this.tb_TOID);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tb_testName);
            this.groupBox1.Controls.Add(this.tb_testID);
            this.groupBox1.Controls.Add(this.cb_testList);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_SID);
            this.groupBox1.Location = new System.Drawing.Point(12, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(746, 268);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // tb_result
            // 
            this.tb_result.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tb_result.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_result.Location = new System.Drawing.Point(491, 118);
            this.tb_result.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_result.Name = "tb_result";
            this.tb_result.Size = new System.Drawing.Size(217, 34);
            this.tb_result.TabIndex = 30;
            // 
            // tb_unit
            // 
            this.tb_unit.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tb_unit.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_unit.Location = new System.Drawing.Point(136, 157);
            this.tb_unit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_unit.Name = "tb_unit";
            this.tb_unit.Size = new System.Drawing.Size(213, 34);
            this.tb_unit.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(410, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 24);
            this.label6.TabIndex = 28;
            this.label6.Text = "Result";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(45, 163);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 24);
            this.label7.TabIndex = 27;
            this.label7.Text = "Unit";
            // 
            // btn_addResult
            // 
            this.btn_addResult.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_addResult.BackColor = System.Drawing.Color.AliceBlue;
            this.btn_addResult.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.btn_addResult.FlatAppearance.BorderSize = 3;
            this.btn_addResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_addResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_addResult.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.btn_addResult.Location = new System.Drawing.Point(491, 163);
            this.btn_addResult.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_addResult.Name = "btn_addResult";
            this.btn_addResult.Size = new System.Drawing.Size(217, 34);
            this.btn_addResult.TabIndex = 26;
            this.btn_addResult.Text = "Add Result";
            this.btn_addResult.UseVisualStyleBackColor = false;
            this.btn_addResult.Click += new System.EventHandler(this.btn_addResult_Click);
            // 
            // tb_TOID
            // 
            this.tb_TOID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tb_TOID.Enabled = false;
            this.tb_TOID.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_TOID.Location = new System.Drawing.Point(491, 24);
            this.tb_TOID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_TOID.Name = "tb_TOID";
            this.tb_TOID.Size = new System.Drawing.Size(217, 34);
            this.tb_TOID.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(359, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 24);
            this.label5.TabIndex = 24;
            this.label5.Text = "Test Order ID:";
            // 
            // tb_testName
            // 
            this.tb_testName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tb_testName.Enabled = false;
            this.tb_testName.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_testName.Location = new System.Drawing.Point(491, 73);
            this.tb_testName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_testName.Name = "tb_testName";
            this.tb_testName.Size = new System.Drawing.Size(217, 34);
            this.tb_testName.TabIndex = 23;
            // 
            // tb_testID
            // 
            this.tb_testID.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tb_testID.Enabled = false;
            this.tb_testID.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_testID.Location = new System.Drawing.Point(136, 112);
            this.tb_testID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_testID.Name = "tb_testID";
            this.tb_testID.Size = new System.Drawing.Size(213, 34);
            this.tb_testID.TabIndex = 22;
            // 
            // cb_testList
            // 
            this.cb_testList.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cb_testList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_testList.FormattingEnabled = true;
            this.cb_testList.Items.AddRange(new object[] {
            "Clinical Chemistry",
            "SarsCov IgG"});
            this.cb_testList.Location = new System.Drawing.Point(136, 73);
            this.cb_testList.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_testList.Name = "cb_testList";
            this.cb_testList.Size = new System.Drawing.Size(213, 30);
            this.cb_testList.TabIndex = 21;
            this.cb_testList.SelectedIndexChanged += new System.EventHandler(this.cb_testList_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(383, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 24);
            this.label4.TabIndex = 12;
            this.label4.Text = "Test Name";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 24);
            this.label3.TabIndex = 11;
            this.label3.Text = "Test ID";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 24);
            this.label2.TabIndex = 10;
            this.label2.Text = "Test List";
            // 
            // dgv_results
            // 
            this.dgv_results.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_results.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_results.Location = new System.Drawing.Point(12, 356);
            this.dgv_results.Name = "dgv_results";
            this.dgv_results.RowHeadersWidth = 51;
            this.dgv_results.RowTemplate.Height = 24;
            this.dgv_results.Size = new System.Drawing.Size(746, 221);
            this.dgv_results.TabIndex = 11;
            this.dgv_results.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_results_CellContentClick);
            // 
            // addResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(770, 597);
            this.ControlBox = false;
            this.Controls.Add(this.dgv_results);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_back);
            this.Name = "addResult";
            this.Text = "addResult";
            this.Load += new System.EventHandler(this.addResult_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_results)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.TextBox tb_SID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_TOID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_testName;
        private System.Windows.Forms.TextBox tb_testID;
        private System.Windows.Forms.ComboBox cb_testList;
        private System.Windows.Forms.TextBox tb_result;
        private System.Windows.Forms.TextBox tb_unit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_addResult;
        private System.Windows.Forms.DataGridView dgv_results;
    }
}