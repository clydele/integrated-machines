﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows.Forms;

namespace OAMS_Interface_VFS
{
   
    public partial class Form1 : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        public Form1()
        {
            InitializeComponent();
        }
        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();


        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            SetConnection();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            string userID = "" ;
            string userName = "";
            string CheckLoginSQL = "Select* from tbl_users WHERE UserID='"+tb_uname.Text+"'AND Pword='"+tb_pword.Text+"'";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = CheckLoginSQL;
            sql_reader = sql_cmd.ExecuteReader();
            if (sql_reader.Read() == true)
            {
                 userID = sql_reader["UserID"].ToString();
                userName = sql_reader["Name"].ToString();
                sql_reader.Close();
                sql_reader.Dispose();
                sql_cmd.Dispose();
                sql_con.Close();
                MessageBox.Show("Login Successful");
                Form ListenerForm = new Listener(userName,userID);
                this.Hide();
                ListenerForm.Show();
            }
            else
            {
                MessageBox.Show("Wrong username or password");
            }    
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
