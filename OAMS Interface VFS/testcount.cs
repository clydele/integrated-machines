﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace OAMS_Interface_VFS
{
    public partial class testcount : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        public testcount()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        public void retrieveTO(string reportDate)
        {
            dgv_testCount.Columns.Clear();
            SetConnection();
            string filterTOSQL = "Select* from tbl_testorder WHERE DTCompleted LIKE '%"+reportDate+"%'";
     
            string whereString = "";
            int i = 0;


            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = filterTOSQL;
            sql_reader = null;
            sql_reader = sql_cmd.ExecuteReader();
      
            while(sql_reader.Read())
            {
                if(String.IsNullOrWhiteSpace(sql_reader["TestID"].ToString())==false)
                {
                    if (i == 0)
                    {
                        whereString += "SpecimenID LIKE \"%" + sql_reader["TestID"].ToString() + "%\"";
                    }
                    else
                    {
                        whereString += " OR  SpecimenID LIKE \"%" + sql_reader["TestID"] + "%\"";
                    }
                }
             
                i++;
          
            }
            sql_reader.Close();
            sql_reader.Dispose();
            sql_con.Close();

       
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string CommandText = "Select uniTestID AS 'Test ID',Count(uniTestID) AS 'Test Count' from tbl_results_order WHERE ("+whereString+") AND LTRIM(RTRIM(uniTestID)) !='' GROUP BY uniTestID";
          
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DataSet DS2 = new DataSet();
            DS2.Reset();
            DB.Fill(DS2);
            DT = DS2.Tables[0];
            dgv_testCount.DataSource = DT;
            sql_con.Close();


            /* SetConnection();
             dgv_testCount.Columns.Clear();
             string filterTOSQL = "Select* from tbl_testorder WHERE DTCompleted LIKE'%"+ reportDate +"%'";
             string[] testorders= { };
             sql_con.Open();
             sql_cmd = sql_con.CreateCommand();
             sql_cmd.CommandText = filterTOSQL;
             sql_reader = sql_cmd.ExecuteReader();


             while(sql_reader.Read())
             {

                 testorders.Append(sql_reader["TestId"].ToString());


             }



             sql_reader.Dispose();
             sql_cmd.Dispose();
             sql_con.Close();*/

            /*
                   SetConnection();
                    sql_con.Open();
                    sql_cmd = sql_con.CreateCommand();
                    string CommandText = "Select* from tbl_results_order WHERE SpecimenID='"+testorders[0]+"'";
                    DB = new SQLiteDataAdapter(CommandText, sql_con);
                    DS.Reset();
                    DB.Fill(DS);
                    DT = DS.Tables[0];
                    dgv_testCount.DataSource = DT;
                    sql_con.Close();*/



        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }

        private void btn_generate_Click(object sender, EventArgs e)
        {
            string TODate = dtp_reportDate.Text;
            retrieveTO(TODate);


            
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
