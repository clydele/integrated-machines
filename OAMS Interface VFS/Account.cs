﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace OAMS_Interface_VFS
{
    public partial class Account : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
   
        public Account(string userName,string userID)
        {
            InitializeComponent();
            tb_userID.Text = userID;
            tb_userName.Text = userName;
        }
        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();


        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }
        private void ChangePassword(string Password)
        {
            sql_con.Open();
            string validate = "Select* from tbl_users WHERE UserID='"+tb_userID.Text+"' AND Pword ='" + tb_curpword.Text + "'";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = validate;
            sql_reader = sql_cmd.ExecuteReader();
            if (sql_reader.Read() == true)
            {
                sql_reader.Close();
                sql_cmd.Dispose();
                string changepass = "UPDATE tbl_users SET Pword='" + Password + "' WHERE UserID='" + tb_userID.Text + "'";
                ExecuteQuery(changepass);
                MessageBox.Show("Password Changed");
                this.Close();
            }
            else
            {
                MessageBox.Show("Wrong Current Password");
                sql_reader.Close();
                sql_cmd.Dispose();
            }
            sql_con.Close();
        }



        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Account_Load(object sender, EventArgs e)
        {
            SetConnection();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(String.Compare(tb_cnewpword.Text,tb_cnewpword.Text)==0)
            {
                ChangePassword(tb_newpword.Text);
            }
            else
            {
                MessageBox.Show("Passwords do not match");
            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
