﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace OAMS_Interface_VFS
{
    public partial class DashBoard : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        public string userName;
        public string userID;
        public DashBoard(string Uname,string UID)
        {
            InitializeComponent();
            userName = Uname;
            userID = UID;
        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }
        private void showResults()
        {
            dgv_orders.Columns.Clear();
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string CommandText = "Select SpecimenID,Priority,SpecimenType AS Specimen from tbl_order_record WHERE Status='Unbound'";
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            dgv_orders.DataSource = DT;
            sql_con.Close();
        }
       private void showTOs()
        {
            dgv_completed.Columns.Clear();
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string CommandText = "Select TOrderID As 'Test Number',PatientID,Name,Age,Gender,TestID As 'Specimen ID' from tbl_testorder WHERE Status='Completed' OR Status='Bound' ORDER BY TOrderID desc";
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DataSet DS2 = new DataSet();
            DS2.Reset();
            DB.Fill(DS2);
            DT = DS2.Tables[0];
            dgv_completed.DataSource = DT;
            sql_con.Close();
        }
            private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
         


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void testOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form TestOrdersF = new TestOrder();
            TestOrdersF.ShowDialog();
          
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string Specimen = dgv_orders.CurrentRow.Cells[0].Value.ToString();
            Form BoundForm = new Bound(Specimen);
            BoundForm.ShowDialog();
        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form TestOrder = new TestOrder();
            TestOrder.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void DashBoard_Load(object sender, EventArgs e)
        {
            showTOs();
            showResults();
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            showTOs();
            showResults();
            
        }

        private void dgv_completed_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string Specimen = dgv_completed.CurrentRow.Cells[5].Value.ToString();
            Form BoundForm = new Bound(Specimen);
            BoundForm.ShowDialog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void boundResultsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void adminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form AdminForm = new Admin();
            AdminForm.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Application.Exit();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void btn_refresh_Click_1(object sender, EventArgs e)
        {

        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form changePass = new Account(userName,userID);
            changePass.ShowDialog();
            
        }

        private void reportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void btn_saveOnly_Click(object sender, EventArgs e)
        {
            showTOs();
            showResults();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(timer1.Enabled==true)
            {
                timer1.Enabled = false;
            }
            else
            {
                timer1.Enabled = true;
            }
        }

        private void testCountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form testCount = new testcount();
            testCount.Show();
        }
    }
}
