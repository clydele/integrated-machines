﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OAMS_Interface_VFS
{
    public partial class print : Form
    {
        ReportDataSource datasource;
        ReportDataSource datasource2;
        public print(ReportDataSource Info,ReportDataSource result,string FirstMt,string SecondMT,string Pathologist)
        {
            datasource = Info;
            datasource2 = result;
            InitializeComponent();
            signImages(FirstMt,SecondMT,Pathologist);
        }
        public void SavePDF(ReportViewer viewer, string savePath)
        {
            LocalReport report = new LocalReport();
            report.ReportPath = "Report1.rdlc";
            ReportDataSource rds = new ReportDataSource();
            report.DataSources.Add(rds);
            Byte[] mybytes = report.Render("WORD");
            //Byte[] mybytes = report.Render("PDF"); for exporting to PDF
            using (FileStream fs = File.Create(@"D:\SalSlip.doc"))
            {
                fs.Write(mybytes, 0, mybytes.Length);
            }
        }
        private void signImages(string FirstSign,string SecondSign,string PathSign)
        {

            
       
            ReportParameter paramLogo = new ReportParameter();
            paramLogo.Name = "FirstMTSign";
            paramLogo.Values.Add(@"file:///" + Application.StartupPath + "/sign_img/" + FirstSign+ ".png");

            ReportParameter paramLogo2 = new ReportParameter();
            paramLogo2.Name = "SecondMTSign";
            paramLogo2.Values.Add(@"file:///"+ Application.StartupPath +"/sign_img/"+ SecondSign + ".png");

            ReportParameter paramLogo3 = new ReportParameter();
            paramLogo3.Name = "PathologistSign";
            paramLogo3.Values.Add(@"file:///" + Application.StartupPath + "/" + PathSign + ".png");



            this.reportViewer1.LocalReport.EnableExternalImages = true;
            this.reportViewer1.LocalReport.SetParameters(paramLogo);
            this.reportViewer1.LocalReport.SetParameters(paramLogo2);
            this.reportViewer1.LocalReport.SetParameters(paramLogo3);
            this.reportViewer1.RefreshReport();
            
        }

        private void print_Load(object sender, EventArgs e)
        {
            MessageBox.Show(Application.StartupPath.ToString());
            this.reportViewer1.RefreshReport();
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(datasource);
            this.reportViewer1.LocalReport.DataSources.Add(datasource2);
            this.reportViewer1.RefreshReport();
            
            
        }


        private void reportViewer1_Load(object sender, EventArgs e)
        {
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            SavePDF(reportViewer1, Application.StartupPath.ToString()) ;
        }

        private void reportViewer1_ReportExport(object sender, ReportExportEventArgs e)
        {

        }
    }
}
