﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace OAMS_Interface_VFS
{
    public partial class authentication : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        public String TestOrder="";
        public String Trans="";
        public authentication(string cTestOrder,string cTrans)
        {
            
            InitializeComponent();
            SetConnection();
            TestOrder = cTestOrder;
            Trans = cTrans;
        }


        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            string authenticationSQL = "Select* from tbl_users WHERE UserID='"+tb_uname.Text+"' and Pword='"+tb_pword.Text+"'AND Name='"+cb_approver.Text+"'";
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = authenticationSQL;
            sql_reader = sql_cmd.ExecuteReader();
            if(sql_reader.Read()==true)
            {
                sql_reader.Close();
                sql_reader.Dispose();
                sql_cmd.Dispose();
                sql_con.Close();
                string InsertLog = "INSERT INTO tbl_approvedLog VALUES(null,'"+TestOrder+"','"+cb_approver.Text+"','"+tb_uname.Text+"','"+Trans+"')";
                ExecuteQuery(InsertLog);
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("wrong username or password");
                sql_reader.Close();
                sql_reader.Dispose();
                sql_cmd.Dispose();
                sql_con.Close();
            }
         
            
        }
        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();


        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }

        private void authentication_Load(object sender, EventArgs e)
        {
            fillComboBox();
        }
        public void fillComboBox()
        {
            String getMTs = "Select* from tbl_users WHERE Type='Medical Technologist'";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = getMTs;
            sql_reader = sql_cmd.ExecuteReader();
            while(sql_reader.Read())
            {
                cb_approver.Items.Add(sql_reader["Name"].ToString().Trim());
            }
            sql_con.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cb_approver_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
