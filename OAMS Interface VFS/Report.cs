﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Windows.Forms;

namespace OAMS_Interface_VFS
{
   
    public partial class Report : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        public Report()
        {
            InitializeComponent();
            SetConnection();
        }
        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();


        }

        private void Report_Load(object sender, EventArgs e)
        {

        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }
        public void retreiveOrders()
        {

        }
        private void retrieveTestOrders()
        {
          

        }

        private void btn_generate_Click(object sender, EventArgs e)
        {
          
            string TestOrdersToday="start";
            string TestOrdersName = "start";
            string TestResult = "start";
            string FBSs = "start";
            string BUNs = "start";
            string CREAs = "start";
            string Lipids = "start";
            string ASTs = "start";
            string ALTs = "start";
            string NAs = "start";
            string Potassiums = "start";
            string fastmovings = "FBS^Urea Nitrogen^Creatinine^Lipid^AST^ALTV^Sodium^Potassium";
            string[] fastmovingarr = fastmovings.Split('^');
            string showresults = "select* from tbl_testorder where DTCompleted LIKE'"+dtp_today.Text+"'";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = showresults;
            sql_reader = null;
            sql_reader = sql_cmd.ExecuteReader();
            while(sql_reader.Read())
            {
                TestOrdersToday += "^ " + sql_reader["TestID"];
                TestOrdersName += "^ " + sql_reader["Name"];

            }
            sql_reader.Close();
            sql_reader.Dispose();
            sql_cmd.Dispose();
            string showAscResults;
            string[] RetrievedTOs = TestOrdersToday.Split('^');
            string[] RetrievedNames = TestOrdersName.Split('^');
            int b=1;
            while(b<RetrievedTOs.Length)
            {
                FBSs += "^ ";
                BUNs += "^ ";
                CREAs += "^ ";
                Lipids += "^ ";
                ASTs += "^ ";
                ALTs += "^ "; ;
                NAs += "^ ";
                Potassiums += "^ ";
                TestResult += "^ ";
                if(String.IsNullOrWhiteSpace(RetrievedTOs[b])==false)
                {
                    int c = 0;
                    while(c<fastmovingarr.Length)
                    {
                        showAscResults = "Select* from tbl_results_order WHERE SpecimenID='" + RetrievedTOs[b].ToString().Trim() + "' AND uniTestID LIKE'"+ fastmovingarr[c].ToString()+"'";
                        sql_cmd = sql_con.CreateCommand();
                        sql_cmd.CommandText = showAscResults;
                        sql_reader = null;
                        sql_reader = sql_cmd.ExecuteReader();
                        if(sql_reader.Read()==true)
                        {
                            if(c == 0)
                            {
                                FBSs += sql_reader["DMV"];
                            }
                            if(c == 1)
                            {
                                BUNs += sql_reader["DMV"];
                            }
                            if (c == 2)
                            {
                                CREAs += sql_reader["DMV"];
                            }
                            if (c == 3)
                            {
                                Lipids += sql_reader["DMV"];
                            }
                            if (c == 4)
                            {
                                ASTs += sql_reader["DMV"];
                            }
                            if (c == 5)
                            {
                                ALTs += sql_reader["DMV"];
                            }
                            if (c == 6)
                            {
                                NAs += sql_reader["DMV"];
                            }
                            if (c == 7)
                            {
                                Potassiums += sql_reader["DMV"];
                            }
                        }
                           
                    
                        sql_reader.Close();
                        sql_reader.Dispose();
                        sql_cmd.Dispose();
                        c++;
                    }
                    
                    showAscResults = "Select* from tbl_results_order WHERE SpecimenID='" + RetrievedTOs[b].ToString().Trim() + "' AND  uniTestID NOT LIKE 'AST' AND uniTestID NOT LIKE 'ALT%' AND uniTestID NOT LIKE 'Sodium' AND uniTestID NOT LIKE 'Potassium' AND uniTestID NOT LIKE 'Urea Nitrogen' AND uniTestID NOT LIKE 'Creatinine'";
                    sql_cmd = sql_con.CreateCommand();
                    sql_cmd.CommandText = showAscResults;
                    sql_reader = null;
                    sql_reader = sql_cmd.ExecuteReader();
                    while (sql_reader.Read())
                    {
                        TestResult += sql_reader["UniTestID"] + "=" + sql_reader["DMV"] + " " + sql_reader["Unit"] + "|";
                    }
                    sql_reader.Close();
                    sql_reader.Dispose();
                    sql_cmd.Dispose();
                }
               
                b++;
            }
            sql_con.Close();
            string[] RetrievedResults = TestResult.Split('^');
            string[] Names = TestOrdersName.Split('^');
            string[] FBS = FBSs.Split('^');
            string[] BUN = BUNs.Split('^');
            string[] CREA = CREAs.Split('^');
            string[] Lipid = Lipids.Split('^'); ;
            string[] AST = ASTs.Split('^'); ;
            string[] ALT = ALTs.Split('^'); ;
            string[] NA= NAs.Split('^'); ;
            string[] Potassium = Potassiums.Split('^');

            DataSet AssaysReport = new DataSet2();
            DataTable printSummary = AssaysReport.Tables[0];
            DataRow pdataRow = printSummary.NewRow();
            int f = 1;
            while (f < RetrievedResults.Length)
            {
                pdataRow["No"] = "";
                pdataRow["Time"] = "";
                pdataRow["AG"] = "";
                pdataRow["Ward"] = "";
                pdataRow["Acc"] = "";
                pdataRow["Name"] =TestOrdersName[f] ;
                pdataRow["FBS"] = FBS[f];
                pdataRow["BUA"] = "";
                pdataRow["BUN"] = BUN[f];
                pdataRow["CREA"] =CREA[f];
                pdataRow["LipidProfile"] = Lipid[f];
                pdataRow["AST"] = AST[f];
                pdataRow["ALT"] = ALT[f];
                pdataRow["NA"] = NA[f];
                pdataRow["K"] = Potassium[f];
                pdataRow["OtherTest"] = RetrievedResults[f];
                printSummary.Rows.Add(pdataRow);
                f++;
            }

            Form printAssaySummary = new printSummary(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", AssaysReport.Tables[0]));
            printAssaySummary.ShowDialog();
            this.Close();





        }
    }
}
