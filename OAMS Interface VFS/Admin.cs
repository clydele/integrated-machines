﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace OAMS_Interface_VFS
{
    public partial class Admin : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        public Admin()
        {
            InitializeComponent();
            SetConnection();
        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }
        private void showUsers()
        {
            
            dgv_users.Columns.Clear();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string CommandText = "Select UserID,Name,Type,LicNo,Signature AS 'Signature Path' from tbl_users";
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            dgv_users.DataSource = DT;
            sql_con.Close();

        
            
        }


        private void ExecuteQuery(string txtQuery)
        {
            
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();


        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btn_saveUser_Click(object sender, EventArgs e)
        {

            try
            {
                string validateRecord = "Select* from tbl_users WHERE Name='" + tb_Uname.Text + "'";
                sql_con.Open();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = validateRecord;
                sql_reader = sql_cmd.ExecuteReader();
                if (sql_reader.Read() == false)
                {
                    sql_cmd.Dispose();
                    sql_reader.Close();
                    sql_con.Close();
                    string FilePath = "";
                    string SaveUser = "INSERT INTO tbl_users VALUES(null,'" + tb_Uname.Text + "','12345','" + cb_userType.Text + "','" + tb_LicNo.Text + "','" + FilePath + "')";
                    ExecuteQuery(SaveUser);
                    MessageBox.Show("User Saved,Default Password is 12345 and needs to be changed upon Login");
                    
                }
                else
                {
                    sql_cmd.Dispose();
                    sql_reader.Close();
                    sql_con.Close();
                }
            }
            catch(Exception err)
            {
                MessageBox.Show(err.ToString());
            }
            showUsers();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                string Filepath = "";
                string Update = "Update tbl_users SET Name='" + tb_Uname.Text + "',Type='" + cb_userType.Text + "',LicNo='" + tb_LicNo.Text + "',Signature='" + Filepath + "'";
                ExecuteQuery(Update);
                MessageBox.Show("Product Successfully Update");
            }
            catch(Exception err)
            {
                MessageBox.Show(err.ToString());
            }
            showUsers();
        }

        private void Admin_Load(object sender, EventArgs e)
        {
            SetConnection();
            showUsers();

        }

        private void dgv_users_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            tb_UID.Text = dgv_users.CurrentRow.Cells[0].Value.ToString();
            tb_Uname.Text = dgv_users.CurrentRow.Cells[1].Value.ToString();
            cb_userType.Text = dgv_users.CurrentRow.Cells[2].Value.ToString();
            tb_LicNo.Text = dgv_users.CurrentRow.Cells[3].Value.ToString();
            btn_update.Enabled = true;
            
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void btn_signature_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
           
            tb_signPath.Text = openFileDialog1.FileName.ToString();
            string ss = tb_signPath.Text;
            pb_signature.ImageLocation = ss.ToString();
            pb_signature.SizeMode = PictureBoxSizeMode.StretchImage;

           

            
           
            
        }

        private void btn_savepic_Click(object sender, EventArgs e)
        {
           
        }
    }
}
