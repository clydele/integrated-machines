﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace OAMS_Interface_VFS
{
    public partial class TestOrder : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        public TestOrder()
        {
            InitializeComponent();
            showTOs();
        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }
        private void showTOs()
        {
            dgv_testorders.Columns.Clear();
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string CommandText = "Select TOrderId, PatientID,Name,RoomNo,Age,Gender,RequestingPhysician,TestId from tbl_testorder WHERE Status='Created' ORDER BY TOrderID desc";
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            dgv_testorders.DataSource = DT;
            sql_con.Close();


            DataGridViewButtonColumn btn_edit = new DataGridViewButtonColumn();
            btn_edit.Name = "btn_edit";
            btn_edit.Text = "/";
            btn_edit.HeaderText = "Edit Order";
            btn_edit.UseColumnTextForButtonValue = true;

            dgv_testorders.Columns.Insert(8, btn_edit);

            DataGridViewButtonColumn btn_deleted = new DataGridViewButtonColumn();
            btn_deleted.Name = "btn deleted";
            btn_deleted.Text = "*";
            btn_deleted.HeaderText = "Delete Order";
            btn_deleted.UseColumnTextForButtonValue = true;
            dgv_testorders.Columns.Insert(9, btn_deleted);

            DataGridViewButtonColumn btn_addTest = new DataGridViewButtonColumn();
            btn_addTest.Name = "btn_addTest";
            btn_addTest.Text = "+";
            btn_addTest.HeaderText = "Add Test";
            btn_addTest.UseColumnTextForButtonValue = true;
            dgv_testorders.Columns.Insert(10, btn_addTest);


        }

        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
            showTOs();


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void TestOrder_Load(object sender, EventArgs e)
        {
            showTOs();
        }
       

        private void btn_save_Click(object sender, EventArgs e)
        {
            string SaveTestOrder = "INSERT INTO tbl_testorder VALUES(null,'"+tb_PID.Text+ "','" + tb_name.Text + "','" + cb_room.Text + "','"+dtp_birthDate.Value+"','"+tb_age.Text+"','"+cb_gender.Text+"','"+tb_physician.Text+"','"+tb_specimenID.Text+"','Created',null,null,null,null,null)";
            ExecuteQuery(SaveTestOrder);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgv_testorders_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            int rowIndex = dgv_testorders.Columns[e.ColumnIndex].Index;
            if (rowIndex==8)
            {
                tb_TID.Text = dgv_testorders.CurrentRow.Cells[0].Value.ToString();
                tb_PID.Text = dgv_testorders.CurrentRow.Cells[1].Value.ToString();
                tb_name.Text = dgv_testorders.CurrentRow.Cells[2].Value.ToString();
                cb_room.Text = dgv_testorders.CurrentRow.Cells[3].Value.ToString();
                tb_age.Text = dgv_testorders.CurrentRow.Cells[4].Value.ToString();
                cb_gender.Text = dgv_testorders.CurrentRow.Cells[5].Value.ToString();
                tb_physician.Text= dgv_testorders.CurrentRow.Cells[6].Value.ToString();
                tb_specimenID.Text= dgv_testorders.CurrentRow.Cells[7].Value.ToString();

            }
            if(rowIndex==9)
            {
                string TID = dgv_testorders.CurrentRow.Cells[0].Value.ToString();
                string SQLDeleteTO = "Delete from tbl_testorder WHERE TOrderID='"+TID+"'";
                ExecuteQuery(SQLDeleteTO);
            }
            if(rowIndex==10)
            {
                
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
