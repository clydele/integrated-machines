﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace OAMS_Interface_VFS
{
    public partial class sarsprint : Form
    {
        ReportDataSource datasource;
        ReportDataSource datasource2;
        public sarsprint(ReportDataSource Info, ReportDataSource result, string FirstMt, string SecondMT, string Pathologist, string value)
        {
            datasource = Info;
            datasource2 = result;
            InitializeComponent();
            interpretation(value);
        }
        private void interpretation(string value)
        {
           
            string analysis = "";
            float resultsss = float.Parse(value);
            if(resultsss>=1)
            {
                analysis = "reactive";
            }
            else
            {
                analysis = "non-reactive";
            }

            ReportParameter paramLogo = new ReportParameter();
            paramLogo.Name = "interpretation";
            paramLogo.Values.Add(analysis);
            this.reportViewer1.LocalReport.SetParameters(paramLogo);
            this.reportViewer1.RefreshReport();

        }


        private void sarsprint_Load(object sender, EventArgs e)
        {
            MessageBox.Show(Application.StartupPath.ToString());
            this.reportViewer1.RefreshReport();
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(datasource);
            this.reportViewer1.LocalReport.DataSources.Add(datasource2);
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
