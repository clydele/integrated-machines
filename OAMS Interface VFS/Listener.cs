﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.IO.Ports;

namespace OAMS_Interface_VFS
{
    public partial class Listener : Form
    {
        delegate void SetTextCallback(string text);
        public string DataBatch =null;
        public string DataReceived;
        public string SpecimenID = "";
        public string SecondBatchFilter;
        public string userName = "";
        public string userID = "";
        public string prevseqNum = "1";
        public string prevFirstChar = null;
        public string patientdatastring = null;
        public string headerddatastring = null;
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        public Listener(string Name,string ID)
        {
            userName = Name;
            userID = ID;
            InitializeComponent();
        }
        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();
            
        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }

        public void serialComEnable()
        {
            string LoadPort = "Select* from tbl_PortConfig";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = LoadPort;
            sql_reader = sql_cmd.ExecuteReader();

          
            try
            {
                serialPort1.PortName = sql_reader["ComPort"].ToString();
                serialPort1.BaudRate = Convert.ToInt32(sql_reader["BaudRate"].ToString());
                serialPort1.DataBits = Convert.ToInt32(sql_reader["Databits"].ToString());
                serialPort1.StopBits = (StopBits)Enum.Parse(typeof(StopBits), sql_reader["StopBits"].ToString());
                serialPort1.Parity = (Parity)Enum.Parse(typeof(Parity), sql_reader["Parity"].ToString());
                serialPort1.DtrEnable = true;
                serialPort1.RtsEnable = true;
                serialPort1.Open();
                MessageBox.Show(sql_reader["ComPort"].ToString() + sql_reader["BaudRate"].ToString() + sql_reader["StopBits"].ToString() + sql_reader["Parity"].ToString() + "Communications Enabled");

            }
            catch (Exception err)
            {
                MessageBox.Show(err.ToString());
            }
            sql_reader.Close();

        }
        private void Listener_Load(object sender, EventArgs e)
        {
            serialComEnable();
            SetConnection();
            this.WindowState = FormWindowState.Minimized;
            Form DashBoardForm = new DashBoard(userName,userID);
            DashBoardForm.Show();
            
        }
        public void revealData()
        {
           
            
            
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            DataReceived = serialPort1.ReadExisting().ToString();
            SetText(DataReceived.ToString());
            try
            {
                byte[] arr = { };
                serialPort1.Read(arr, 0,arr.Length);
                MessageBox.Show(arr.Length.ToString());
                if(arr.Length>=0)
                {
                    if (arr[0] == 5 | arr[0] == 2)
                    {
                        byte[] buffer = { 0x06 };
                        serialPort1.Write(buffer, 0, buffer.Length);

                    }
                    else
                    {

                    }
                }
                
                
            }
            catch(Exception err)
            {
                
            }
            
               
              

            /* byte[] arr = Encoding.ASCII.GetBytes(serialPort1.ReadExisting());

             byte[] buffer = { 0x06 };
             DataReceived = serialPort1.ReadExisting().ToString();
             byte packet = arr[0];

                 if(packet>=0 & packet<=8 )
                 {
                     serialPort1.Write(buffer, 0, buffer.Length);
                     SetText(DataReceived);
                 }
                 else
                 {
                     SetText(DataReceived);
                     revealData();
                 }


             //foreach (byte packet in arr)
             //{
                 if (packet >= 1 & packet <= 8)
                 {
                     serialPort1.Write(buffer, 0, buffer.Length);
                     SetText("\n");


                 }
                 else
                 {

                 }
             //}
             SetText(DataReceived.ToString());
            */






            /* byte[] arr = Encoding.ASCII.GetBytes(serialPort1.ReadExisting().ToString());
             if (arr[0] == 5)
             {
            byte[] arr = Encoding.ASCII.GetBytes(serialPort1.ReadExisting().ToString());
            int i = 0;
            while(i<arr.Length)
            {
                if(arr[i]==5)
                {
                    byte[] buffer = { 0x06 };
                    serialPort1.Write(buffer, 0, buffer.Length);
                }
                i++;
            }
            /*
                byte[] buffer = { 0x06 };
                serialPort1.Write(buffer, 0, buffer.Length);
            

            /*}
            else
            {

            }

            /*
            if(arr[0]>0 && arr[0]<7)
            {
                SetText("\n"+DataReceived.ToString());
            }
            */

            /*try
            {
                string[] Divisor = DataReceived.Split('|');
                string firstReceived = Divisor[0].Substring(Divisor[0].Length - 1, 1).ToString();

                if (String.Compare(firstReceived, "H") == 0 || String.Compare(firstReceived, "R") == 0 || String.Compare(firstReceived, "O") == 0 || String.Compare(firstReceived, "R") == 0 || String.Compare(firstReceived, "L") == 0)
                {
                    SetText("\n" + DataReceived.ToString());
                }
                else
                {
                    SetText(DataReceived.ToString());
                }
            }
            catch
            {

            }*/
            // byte[] buffer = { 0x06 };
            // serialPort1.Write(buffer, 0, buffer.Length);
            //  DataReceived = serialPort1.ReadLine().ToString();
            //string DTR = serialPort1.ReadExisting().ToString().Substring(0,1);
            // byte[] DataSignal= Convert.FromBase64String(DTR);
            // SetText(DataReceived);


            //Working Code


            /*byte[] buffer = { 0x06 };
            serialPort1.Write(buffer, 0, buffer.Length);
            DataReceived = serialPort1.ReadExisting().ToString();
            SetText(DataReceived.ToString());
            
            */

        }

        private void newSetText(string text)
        {
            string FirstChar;
            string seqNumber = "0";
            if (this.rtb_received.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(newSetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                DataBatch += text;
                rtb_received.Text += text;
            }
        }
        private void SetText(string text)

        {
          
     

           
            string FirstChar;
            string seqNumber="0";
            if (this.rtb_received.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {

                string[] receivedInfo = text.Split('|');

                if (String.IsNullOrEmpty(receivedInfo[0].ToString()) == false)
                {
                    FirstChar = receivedInfo[0].Substring(receivedInfo[0].Length - 1, 1);


                    if (String.Compare(FirstChar, "H") == 0)
                    {


                        if (String.IsNullOrEmpty(DataBatch) == true)
                        {
                            DataBatch += text;

                        }
                        else
                        {
                            //GetOrder(DataBatch);
                            DataBatch += text;

                        }


                    }
                  
                  
                    else if (String.Compare(FirstChar, "L") == 0)
                    {
                        if (String.IsNullOrEmpty(DataBatch) == true)
                        {
                            DataBatch += text;
                        }
                        else
                        {
                            DataBatch += text;
                            GetOrder(DataBatch);
                            
                      
                        }
                    }
                    else if(string.Compare(FirstChar,"R")==0)
                    {
                        DataBatch += "\n" + text;
                        rtb_received.Text += "\n" + text;
                    }
                    else
                    {
                        rtb_received.Text += text;
                        DataBatch += text;
                    }



                }



            }
        }
        public void GetOrder(string BatchData)
        {
            
            if (String.IsNullOrEmpty(SpecimenID) == true)
            {
                string[] BatchLineRecords;
                string[] BatchLines = BatchData.Split('\n');
                string BLRFC;
                string[] BLRFCte;
                for (int b = 0; b < BatchLines.Length; b++)
                {
                    if (String.IsNullOrEmpty(BatchLines[b].ToString()) == false)
                    {

                        BatchLineRecords = BatchLines[b].ToString().Split('|');
                        BLRFC = BatchLineRecords[0].ToString().Substring(BatchLineRecords[0].ToString().Length - 1, 1);
                        
                        if (String.Compare(BLRFC, "O") == 0)
                        {
                            BLRFCte = BatchLineRecords[2].Split('^');
                            SpecimenID = BLRFCte[0].ToString();

                            DataBatchFilter(BatchData);
                      
                            

                        }
                    }
                    

                }

            }
            else
            {
                string[] BatchLineRecords;
                string[] BatchLines = BatchData.Split('\n');
                string BLRFC;
                string[] BLRFCte;
                for (int b = 0; b < BatchLines.Length; b++)
                {
                    if (String.IsNullOrEmpty(BatchLines[b].ToString()) == false)
                    {

                        BatchLineRecords = BatchLines[b].ToString().Split('|');
                        BLRFC = BatchLineRecords[0].ToString().Substring(BatchLineRecords[0].ToString().Length - 1, 1);

                        if (String.Compare(BLRFC, "O") == 0)
                        {
                            BLRFCte = BatchLineRecords[2].Split('^');
                            SpecimenID = BLRFCte[0].ToString();
                            DataBatchFilter(BatchData);

                 

                        }
                    }


                }

                DataBatchFilter(BatchData);
            }
        }
        public void DataBatchFilter(string rawdata)
        {
          
            string[] RecordFilter = rawdata.Split('\n');
            int i = 0;
            string currentLine = "";


            while (i < RecordFilter.Length)
            {
                currentLine = RecordFilter[i].ToString();
                
                if (String.IsNullOrEmpty(currentLine) == false || String.IsNullOrWhiteSpace(currentLine) == false)
                {
                    string[] currentLineFields = currentLine.Split('|');
                    string currentLineFirstRecord = currentLineFields[0].Substring((currentLineFields[0].Length) - 1, 1);
                    if (String.Compare(currentLineFirstRecord, "H") == 0)
                    {
                        headerfilter(currentLine.ToString());
                    }
                    else if (String.Compare(currentLineFirstRecord, "P") == 0)
                    {
                        patientfilter(currentLine.ToString());
                    }
                    else if (String.Compare(currentLineFirstRecord, "O") == 0)
                    {
                        orderfilter(currentLine.ToString());
                    }
                    else if(String.Compare(currentLineFirstRecord,"C")== 0)
                    {

                    }
                    else if (String.Compare(currentLineFirstRecord, "R") == 0)
                    {
                        resultfilter(currentLine.ToString());
                    }
                    else if(String.Compare(currentLineFirstRecord,"L")==0)
                    {
                        DataBatch = "";
                       
                    }
                    else
                    {
                        
                    }
                    
                }

                else
                {

                }

                i++;
            }
           
           
           


        }
        public void headerfilter(string headerString)
        {

            string[] headerfields = headerString.Split('|');
            string headerChar = headerfields[0].Substring(headerfields[0].Length - 1, 1);
            string SendName = headerfields[4].ToString();
            string versionNumber = headerfields[12].ToString();
            string DaTMessage = headerfields[13].ToString();
            string saveHeaderFilter = "INSERT INTO tbl_header_record VALUES(null,'" + SendName.ToString() + "','" + versionNumber.ToString() + "','" + DaTMessage.ToString() + "','" + SpecimenID + "')";
            ExecuteQuery(saveHeaderFilter);




        }
        public void patientfilter(string patientString)
        {
            
            string[] patientfields = patientString.Split('|');
            string recordType;
            string seqNumber;
            string practiceAssignId;
            string PatientName;
            string BirthDate;
            string Sex;
            string Address;
            string PhysicianID;
            string SpecialField;
            string Location;
            try { recordType = patientfields[0].Substring(patientfields[0].Length - 1, 1); } catch { recordType = ""; }
            try { seqNumber = patientfields[1].ToString(); } catch { seqNumber = ""; }
            try { practiceAssignId = patientfields[5].ToString(); } catch { practiceAssignId = ""; }
            try { PatientName = patientfields[2].ToString(); } catch { PatientName = ""; }
            try { BirthDate = patientfields[7].ToString(); } catch { BirthDate = ""; }
            try { Sex = patientfields[8].ToString(); } catch { Sex = ""; }
            try { Address = patientfields[10].ToString(); } catch { Address = ""; }
            try { PhysicianID = patientfields[13].ToString(); } catch { PhysicianID = ""; }
            try { SpecialField = patientfields[4].ToString(); } catch { SpecialField = ""; }
            try { Location = patientfields[25].ToString(); } catch { Location = ""; }
            

            int existing = 0;
            string validateOrder = "Select* from tbl_patient_record WHERE SeqNumber='" + seqNumber + "' AND specimenID='" + SpecimenID + "'";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = validateOrder;
            sql_reader = sql_cmd.ExecuteReader();
            sql_reader.Read();
            if (String.IsNullOrEmpty(sql_reader["SpecimenID"].ToString()) == true)
            {
                existing = 1;
            }
            else
            {
                existing = 0;
            }
            sql_reader.Close();
            sql_cmd.Dispose();
            sql_con.Close();
            if (existing == 1)
            {
                string savePatientFilter = "INSERT INTO tbl_patient_record VALUES(null,'" + seqNumber.ToString() + "','" + PatientName + "','" + practiceAssignId.ToString() + "','" + BirthDate.ToString() + "','" + Sex.ToString() + "','" + Address.ToString() + "','" + PhysicianID.ToString() + "','" + SpecialField.ToString() + "','" + Location.ToString() + "','" + SpecimenID + "')";
                ExecuteQuery(savePatientFilter);

            }
            patientdatastring = null;



        }
        
        public void orderfilter(string orderString)
        {

            //added code
            

            //original code
            string[] orderfields = orderString.Split('|');
            string recordType;
            string SeqNumber;
         
            string UniTestID;
            string Priority;
            string SpecimenCollect;
            string ActionCode;
            string SpecimenType;
            string RecordType;
           

            try { recordType = orderfields[0].Substring(orderfields[0].Length - 1, 1); } catch { recordType = ""; }
            try { SeqNumber = orderfields[1].ToString(); } catch { SeqNumber = ""; }
            try { UniTestID = orderfields[4].ToString(); } catch { UniTestID = ""; }
            try { Priority = orderfields[5].ToString(); } catch { Priority = ""; }
            try { SpecimenCollect = orderfields[7].ToString(); } catch { SpecimenCollect = ""; }
            try { ActionCode = orderfields[11].ToString(); } catch { ActionCode = ""; }
            try { SpecimenType = orderfields[15].ToString(); } catch { SpecimenType = ""; }
            try { RecordType = orderfields[25].ToString(); } catch { RecordType = ""; }

            int existing = 0;
            string validateOrder = "Select* from tbl_order_record WHERE seqNumber='"+SeqNumber.ToString()+"' AND SpecimenID='"+SpecimenID+"'";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = validateOrder;
            sql_reader = sql_cmd.ExecuteReader();
            sql_reader.Read();
            if(String.IsNullOrEmpty(sql_reader["SpecimenID"].ToString())==true)
            {
                existing = 1;
            }
            else
            {
                existing = 0;
            }
            sql_reader.Close();
            sql_cmd.Dispose();
            string checkTOR = "Select* from tbl_testorder WHERE TestId='"+SpecimenID+"'";
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = checkTOR;
            sql_reader = sql_cmd.ExecuteReader();
            sql_reader.Read();
            if(String.IsNullOrEmpty(sql_reader["TestId"].ToString())==true)
            {
                
            }
            else
            {
                existing = 2;
            }
            
            sql_con.Close();
           
            if(existing==1)
            {
                
                string saveOrderFilter = "INSERT INTO tbl_order_record VALUES(null,'" + SeqNumber.ToString() + "','" + SpecimenID.ToString() + "','" + UniTestID.ToString() + "','" + Priority.ToString() + "','" + SpecimenCollect.ToString() + "','" + ActionCode.ToString() + "','" + SpecimenType.ToString() + "','" + RecordType.ToString() + "','Unbound')";
                ExecuteQuery(saveOrderFilter);
            }
           


           


        }
        public void resultfilter(string resultString)
        {


            
            string[] resultfields = resultString.Split('|');
            string RecordType;
            string seqNumber;
            string uniTestID;
            string DMV;
            string Unit;
            string ResultFlags;
            string ResultStats;
            string OpID;
            string Dstart;
            string Dcompleted;
            string TestCode;

            try { RecordType = resultfields[0].Substring(resultfields[0].Length - 1, 1); } catch { RecordType = ""; }
            try { seqNumber = resultfields[1].ToString(); } catch { seqNumber = ""; }
            try
            {
                uniTestID = resultfields[2].ToString();
                string[] TestArray = uniTestID.Split('+');
                string NumberCode = TestArray[1].ToString();
                string ccsql = "Select* from tbl_analytes WHERE TestCode='" + NumberCode + "'";
                SetConnection();
                sql_con.Open();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = ccsql;
                sql_reader = sql_cmd.ExecuteReader();
                sql_reader.Read();
                TestCode = sql_reader["TestName"].ToString();
                sql_reader.Close();   
                sql_cmd.Dispose();
                sql_con.Close();
            }
            catch { uniTestID = "NA"; TestCode = "NA"; }
            try { DMV = resultfields[3].ToString(); } catch { DMV = ""; }
            try { Unit = resultfields[4].ToString(); } catch { Unit = ""; }
            try { ResultFlags = resultfields[6].ToString(); } catch { ResultFlags = ""; }
            try { ResultStats = resultfields[8].ToString(); } catch { ResultStats = ""; }
            try { OpID = resultfields[10].ToString(); } catch { OpID = ""; }
            try { Dstart = resultfields[11].ToString(); } catch { Dstart = ""; }
            try { Dcompleted = resultfields[2].ToString(); } catch { Dcompleted = ""; }


            if(String.IsNullOrEmpty(uniTestID)==false & String.IsNullOrWhiteSpace(uniTestID)==false & uniTestID!="950" & uniTestID !="951" & uniTestID !="952")
            {
                int existing = 0;
                string validateOrder = "Select* from tbl_results_order WHERE seqNumber='"+seqNumber.ToString()+"' AND SpecimenID ='" + SpecimenID + "' AND uniTestID ='"+TestCode+"'";
               
                SetConnection();
                sql_con.Open();
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = validateOrder;
                sql_reader = sql_cmd.ExecuteReader();
                sql_reader.Read();
                if (String.IsNullOrWhiteSpace(sql_reader["SpecimenID"].ToString()) == true)
                {
                    existing = 1;
                }
                else if(String.IsNullOrEmpty(sql_reader["SpecimenID"].ToString())==true)
                {
                    existing = 1;
                }
                else
                {
                    existing = 0;
                }

                sql_reader.Close();
                sql_cmd.Dispose();
                sql_con.Close();
                if (existing == 1)
                {

                    string saveResultFilter = "INSERT INTO tbl_results_order VALUES(null,'" + seqNumber.ToString() + "','" + TestCode.ToString() + "','" + DMV.ToString() + "','" + Unit.ToString() + "','" + ResultFlags.ToString() + "','" + ResultStats.ToString() + "','" + OpID.ToString() + "',date(),date(),'" + SpecimenID + "',null,null)";
                    ExecuteQuery(saveResultFilter);

                }
                else
                {
                    
                }
            }
            


        }

        private void rtb_received_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            
           

        }

        private void button1_Click(object sender, EventArgs e)
        {
            byte[] buffer = { 0x06 };
            serialPort1.Write(buffer, 0, buffer.Length);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
