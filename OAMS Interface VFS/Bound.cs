﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace OAMS_Interface_VFS
{
    public partial class Bound : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        private string SpecimenID;
        public Bound(string throwSpecimen)
        {
            SpecimenID = throwSpecimen;
            InitializeComponent();
        }
        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();


        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }

        private void Bound_Load(object sender, EventArgs e)
        {
            //DD/MM/YYYYY HH:MM:SS

            dtp__collected.CustomFormat = "MM/dd/yyyy";
            dtp_performed.CustomFormat = "MM/dd/yyyy";
            selectedPathologist();         
            tb_SID.Text = SpecimenID;
            getTestOrder();
            updateRange();
            showResults();
            getSpecimenCollectTime();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        //TO Functions
        public void boundOnly()
        {
            string boundTO = "INSERT INTO tbl_testorder VALUES(null,'" + tb_PID.Text + "','" + tb_PName.Text + "','" + tb_roomNo.Text + "','" + dtp_bdate.Value.ToString() + "','" + tb_Age.Text + "','" + cb_gender.Text + "','" + tb_physician.Text + "','" + tb_SID.Text + "','Bound','null','null','null','null','null','null','" + rtb_remarks.Text + "','null','null')";
            ExecuteQuery(boundTO);
        }
        public void updateBound()
        {
            string updateTO = "UPDATE tbl_testorder SET PatientID='" + tb_PID.Text + "',Name='" + tb_PName.Text + "',RoomNo='" + tb_roomNo.Text + "',Birthdate='" + dtp_bdate.Value + "',Age='" + tb_Age.Text + "',Gender='" + cb_gender.Text + "',RequestingPhysician='" + tb_physician.Text + "',TestId='" + tb_SID.Text + "',Status='Bound',MedTech='" + tb_medtech.Text + "',MLicense='" + tb_licNo.Text + "',Pathologist='" + tb_pathologist.Text + "',Plicense='" + tb_PLicNo.Text + "',LastComment='" + rtb_remarks.Text + "',secMedtech='" + tb_medtech2.Text + "',secMedTechLic='" + tb_licNo2.Text + "',DTCompleted='" + dtp_performed.Text + "' WHERE TOrderID='" + tb_TID.Text + "'";
            ExecuteQuery(updateTO);
        }
        public void saveTO()
        {
            string saveTO = "INSERT INTO tbl_testorder VALUES(null,'"+tb_PID.Text+"','"+tb_PName.Text+"','"+tb_roomNo.Text+"','"+dtp_bdate.Value.ToString()+"','"+tb_Age.Text+"','"+cb_gender.Text+"','"+tb_physician.Text+"','"+tb_SID.Text+"','Completed','"+tb_medtech.Text+"','"+tb_licNo.Text+"','"+tb_medtech2.Text+"','"+tb_licNo2.Text+"','"+tb_pathologist.Text+"','"+tb_PLicNo.Text+"','"+rtb_remarks.Text+"','"+dtp_performed.Text+"','"+dtp__collected.Text+"')";
            ExecuteQuery(saveTO);
        }
        public void updateTO()
        {
            string updateTO = "UPDATE tbl_testorder SET PatientID='" + tb_PID.Text + "',Name='" + tb_PName.Text + "',RoomNo='" + tb_roomNo.Text + "',Birthdate='" + dtp_bdate.Value + "',Age='" + tb_Age.Text + "',Gender='" + cb_gender.Text + "',RequestingPhysician='" + tb_physician.Text + "',TestId='" + tb_SID.Text + "',Status='Completed',MedTech='" + tb_medtech.Text + "',MLicense='" + tb_licNo.Text + "',Pathologist='" + tb_pathologist.Text + "',Plicense='" + tb_PLicNo.Text + "',LastComment='"+rtb_remarks.Text+"',secMedtech='"+tb_medtech2.Text+"',secMedTechLic='"+tb_licNo2.Text+"',DTCompleted='"+dtp_performed.Text+"' WHERE TOrderID='" + tb_TID.Text + "'";
            ExecuteQuery(updateTO);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            
            int existing = 0;
            string validate = "Select* from tbl_testorder WHERE TOrderID='"+tb_TID.Text+"'";
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = validate;
            sql_reader = sql_cmd.ExecuteReader();
            sql_reader.Read();
            if (String.IsNullOrEmpty(sql_reader["TestID"].ToString()) == true)
            {
                existing = 1;
            }
            else
            {
                existing = 0;
            }
            sql_reader.Close();
            sql_cmd.Dispose();
            sql_con.Close();
            
                if (existing == 1)
                {
                    saveTO();
                }
                else
                {
                    updateTO();
                }
                String BoundOrder = "UPDATE tbl_order_record SET  Status='Bound' WHERE SpecimenID='" + SpecimenID + "'";
                ExecuteQuery(BoundOrder);
                CreateDataTable1(tb_PID.Text, tb_PName.Text, tb_roomNo.Text, dtp_bdate.Text, tb_Age.Text, cb_gender.Text, tb_physician.Text, tb_medtech.Text, tb_licNo.Text, tb_pathologist.Text, tb_PLicNo.Text, rtb_remarks.Text, dtp_performed.Value.ToString(), tb_medtech2.Text, tb_licNo2.Text, dtp__collected.Value.ToString());

                this.Refresh();
                
            
          


        }
        public void updateRange()
        {

            List<string> testedAssays=new List<string>();
            string validate = "Select* from tbl_results_order WHERE SpecimenID='" + tb_SID.Text + "'";
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = validate;
            sql_reader = sql_cmd.ExecuteReader();
            while(sql_reader.Read())
            {
                testedAssays.Add(sql_reader["uniTestID"].ToString()) ;
            }
            sql_reader.Close();
            sql_cmd.Dispose();
            
            string[] testedAssaysName = testedAssays.ToArray();
            List<string> testedSIRanges = new List<string>();
            List<string> testedConvRanges = new List<string>();
            int a = 0;
            string getRange="";
            
            while(a<testedAssaysName.Length)
            {
                getRange = "Select* from  tbl_ref_range where Test='"+testedAssaysName[a].ToString()+"'";
                sql_cmd = sql_con.CreateCommand();
                sql_cmd.CommandText = getRange;
                sql_reader = sql_cmd.ExecuteReader();
                if(sql_reader.Read()==true)
                {
                    testedSIRanges.Add(sql_reader["SerumSI"].ToString());
                    testedConvRanges.Add(sql_reader["SerumConv"].ToString());
                }
                else
                {
                    testedSIRanges.Add(" ");
                    testedConvRanges.Add(" ");
                }
                sql_reader.Close();
                sql_cmd.Dispose();
                a++;
            }
            string[] SIRanges = testedSIRanges.ToArray();
            string[] ConvRanges = testedConvRanges.ToArray();
            sql_con.Close();

            string updateTestRangeSQL="";
            int b = 0;
           
            while(b<testedAssaysName.Length)
            {
                updateTestRangeSQL = "UPDATE tbl_results_order SET SIRange='"+SIRanges[b]+"',ConvRange='"+ConvRanges[b]+"' WHERE SpecimenID='"+SpecimenID+"' AND uniTestID='"+testedAssaysName[b]+"'";
                ExecuteQuery(updateTestRangeSQL);
                b++;
            }

        }
        public void CreateDataTable1(string PatientID, string Name, string roomNo, string Bdate, string age, string gender, string physician, string medtech, string medtechlicense, string pathologist, string pathologistlicense, string LastComment, string dtperformed,string secMedTech,string secMedTecLic,string DTCollected)
        {
            DataSet ds = new DataSet1();
            DataTable dtable = ds.Tables[1];
            DataRow row = dtable.NewRow();
            row["PatientID"] = PatientID;
            row["Name"] = Name;
            row["RoomNo"] = roomNo;
            row["Birthdate"] = Bdate;
            row["Age"] = age;
            row["Gender"] = gender;
            row["RequestingPhysician"] = physician;
            row["MedTech"] = medtech;
            row["MLicensee"]=medtechlicense;
            row["Pathologist"] = pathologist;
            row["PLicense"] = pathologistlicense;
            row["DTPerformed"] = dtperformed;
            row["MedTechSec"] = secMedTech;
            row["MTSLicense"] = secMedTecLic;
            row["RemarksMain"] = LastComment;
            row["DTCollected"] = DTCollected;
            dtable.Rows.Add(row);
            string covVal = "";

            DataTable dtable2 = ds.Tables[0];
            int d = -1;
            string prints = "";
            foreach (DataGridViewRow row2 in dgv_results.Rows)
            {
                d++;
               
                    if (d == dgv_results.Rows.Count - 1)
                    {
                        
                    }
                    else
                    {
                        if(row2.Cells[5].Value!=null)
                        {
                            prints = row2.Cells[5].Value.ToString();
                        }
                        
                        

                  
                        
                            if(String.Compare(prints,"checked")==0)
                           {
                                DataRow dRow = dtable2.NewRow();


                                foreach (DataGridViewCell cell in row2.Cells)
                                {
                                    if(cell.ColumnIndex!=5)
                                    {
                                        dRow[cell.ColumnIndex] = dgv_results.Rows[d].Cells[cell.ColumnIndex].Value.ToString();
                                        covVal = dgv_results.Rows[0].Cells[1].Value.ToString();
                                    }
                                   
                                }
                                dtable2.Rows.Add(dRow);

                            }
                            else
                            {
                                 
                            }
                    



                    }
                
            
            
            }
            if(string.Compare(cb_reportForm.Text, "SarsCov IgG")==0)
            {
                Form printform = new sarsprint(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", ds.Tables[1]), new Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", ds.Tables[0]), tb_licNo.Text, tb_licNo2.Text, tb_PLicNo.Text, covVal);
                printform.ShowDialog();
                this.Close();
            }
            else
            {
                Form printform = new print(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", ds.Tables[1]), new Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", ds.Tables[0]), tb_licNo.Text, tb_licNo2.Text, tb_PLicNo.Text);
                printform.ShowDialog();
                this.Close();
            }
            
           


        }
        private void showResults()
        {
            dgv_results.Columns.Clear();
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string CommandText = "Select uniTestID  As 'Test',DMV As 'Result',Unit,SIRange As 'SI Range',ConvRange As 'Conv Range' from tbl_results_order WHERE SpecimenID='"+SpecimenID.ToString()+"' AND uniTestID !=''"; 
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            dgv_results.DataSource = DT;
            sql_con.Close();

            DataGridViewCheckBoxColumn toPrint = new DataGridViewCheckBoxColumn();
            toPrint.Name = "toPrint";
            toPrint.HeaderText = "Print?";
            toPrint.TrueValue = "checked";
            toPrint.FalseValue = "unchecked";
            toPrint.ValueType = typeof(string);


            dgv_results.Columns.Add(toPrint);

        }

        private void fillTextBoxes()
        {
            tb_SID.Text = SpecimenID.ToString();
           
        }
        public void getTestOrder()
        {
            string checkTO = "Select* from tbl_testorder where TestID='"+SpecimenID+"'";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = checkTO;
            sql_reader = sql_cmd.ExecuteReader();
            if(sql_reader.Read()==true)
            {
                tb_TID.Text = sql_reader["TOrderID"].ToString();
                tb_PID.Text= sql_reader["PatientID"].ToString();
                tb_PName.Text= sql_reader["Name"].ToString();
                tb_roomNo.Text= sql_reader["RoomNo"].ToString();        
                dtp_bdate.Value = DateTime.Parse(sql_reader["Birthdate"].ToString());
                tb_Age.Text= sql_reader["Age"].ToString();
                cb_gender.Text = sql_reader["Gender"].ToString();
                tb_physician.Text = sql_reader["RequestingPhysician"].ToString();
                tb_medtech.Text = sql_reader["MedTech"].ToString();
                tb_licNo.Text = sql_reader["MLicense"].ToString();
                tb_medtech2.Text = sql_reader["secMedtech"].ToString();
                tb_licNo2.Text = sql_reader["secMedTechLic"].ToString();
                tb_pathologist.Text = sql_reader["Pathologist"].ToString();
                tb_PLicNo.Text = sql_reader["PLicense"].ToString();
                rtb_remarks.Text = sql_reader["LastComment"].ToString();
                
                if (String.IsNullOrEmpty(sql_reader["DTCompleted"].ToString().Trim())==false & String.Compare(sql_reader["DTCompleted"].ToString(),"null")!=0) 
                {
                    dtp_performed.Enabled = false;
                    dtp_performed.Value = DateTime.Parse(sql_reader["DTCompleted"].ToString().Trim());
                }
                if(tb_medtech.Text != "") { tb_medtech.Enabled = false; }
                
                 
                sql_reader.Close();
                sql_cmd.Dispose();
                sql_con.Close();

            }
            else
            {
                sql_reader.Close();
                sql_cmd.Dispose();
                sql_con.Close();
                getPatient();
            }
            
            string getResultsSQL = "Select* from tbl_results_order WHERE SpecimenID='"+tb_SID.Text+"'";
        }
        public void selectedPathologist()
        {
         
        }

        public void getPatient()
        {
         
            string checkTO = "Select* from tbl_patient_record where specimenID='" + tb_SID.Text + "'";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = checkTO;
            sql_reader = sql_cmd.ExecuteReader();
            if (sql_reader.Read()==true)
            {
                try { tb_PID.Text = sql_reader["praticeAssigId"].ToString(); } catch { tb_PID.Text = "na"; }
                try {
                    string[] PatientNameRaw = sql_reader["PatientName"].ToString().Split('^');
                    string PatientNameNet = PatientNameRaw[0] +","+PatientNameRaw[1]+" "+PatientNameRaw[2]+".";                
                    tb_PName.Text = PatientNameNet.ToString().Trim();


                } 
                catch { tb_PName.Text = "na"; }
                try { cb_gender.Text = sql_reader["Sex"].ToString(); } catch { cb_gender.SelectedIndex = 0; }
                try
                {
                    string colData = sql_reader["BirthDate"].ToString();
                    string birthdayText = colData.Substring(6, 2) + "/" + colData.Substring(4, 2) + "/" + colData.Substring(0, 4);
                    dtp_bdate.Value = DateTime.Parse(birthdayText);
                    TimeSpan AgeR = DateTime.Today-DateTime.Parse(birthdayText);
                    double Ages = AgeR.TotalDays / 365;
                    string[] AgeAr = Ages.ToString().Split('.');
                    string Age = AgeAr[0];
                    tb_Age.Text = Age.ToString();
                }
                catch(Exception err)
                {
                    
                }
                try {
                    string[] physicianName = sql_reader["PhysicianID"].ToString().Split('^');
                    tb_physician.Text = " "+physicianName[0].ToString();
                    
                } 
                catch { tb_physician.Text = ""; }
                try { tb_roomNo.Text = sql_reader["Location"].ToString(); } catch { tb_roomNo.Text = ""; }

                sql_reader.Close();
                sql_cmd.Dispose();
                sql_con.Close();
               

            }
            else
            {
                MessageBox.Show("No Record retrieved" + tb_SID.Text );
                sql_reader.Close();
                sql_cmd.Dispose();
                sql_con.Close();

            }

          

        }
        public void getSpecimenCollectTime()
        {
            string getOrderRecord = "Select* from tbl_order_record WHERE SpecimenID='"+tb_SID.Text+"'";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = getOrderRecord;
            sql_reader = sql_cmd.ExecuteReader();
            if(sql_reader.Read()==true)
            {

                string colData = sql_reader["SpecimenCollect"].ToString().Trim();
                if(String.IsNullOrEmpty(colData)==false )
                {
                    string colDate;
                    try
                    {
                        colDate = colData.Substring(6, 2) + "/" + colData.Substring(4, 2) + "/" + colData.Substring(0, 4) + " " + colData.Substring(8, 2) + ":" + colData.Substring(10, 2) + ":" + colData.Substring(12, 2);
                        dtp__collected.Value = DateTime.Parse(colDate);
                    }
                    catch
                    {

                    }
                        
                }
                
            }
            sql_reader.Close();
            sql_cmd.Dispose();

            sql_con.Close();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void tb_licNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgv_results_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           ;
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        

        private void tb_medtech2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] LicenseNo = { "0075825", "0080991", "0091420", "0094512", " ", "0094011", "0093277", "0081470", "0099737", "0099533", "0098584", "0097950", "0094627", "0077928" };
            int selectedMedtech = tb_medtech2.SelectedIndex;
            tb_licNo2.Text = LicenseNo[selectedMedtech].ToString();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            int existing = 0;
            string validate = "Select* from tbl_testorder WHERE TOrderID='" + tb_TID.Text + "'";
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = validate;
            sql_reader = sql_cmd.ExecuteReader();
            sql_reader.Read();
            if (String.IsNullOrEmpty(sql_reader["TestID"].ToString()) == true)
            {
                existing = 1;
            }
            else
            {
                existing = 0;
            }
            sql_reader.Close();
            sql_cmd.Dispose();
            sql_con.Close();
            if (existing == 1)
            {
                boundOnly();
                MessageBox.Show("Successfully Bound!");
              
            }
            else
            {
                updateBound();
                MessageBox.Show("Successfully Bound!");
            }
            String BoundOrder = "UPDATE tbl_order_record SET  Status='Bound' WHERE SpecimenID='" + SpecimenID + "'";
            ExecuteQuery(BoundOrder);
            
            this.Refresh();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_saveOnly_Click(object sender, EventArgs e)
        {

        }

        private void tb_medtech_TextChanged(object sender, EventArgs e)
        {

        }

        private void tb_PLicNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void tb_medtech_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(tb_medtech.SelectedIndex==0)
            {
                tb_licNo.Text = "0081161";
            }
            if (tb_medtech.SelectedIndex == 1)
            {
                tb_licNo.Text = "0101418";
            }
            if (tb_medtech.SelectedIndex == 2)
            {
                tb_licNo.Text = "0082967";
            }
        }

        private void btn_addResult_Click(object sender, EventArgs e)
        {
            Form addResult = new addResult(SpecimenID.ToString());
            addResult.Show();
            this.Close();
        }
        public void showAnalytes()
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            int i = 0;
            foreach(DataGridViewRow row in dgv_results.Rows)
            {
                i++;
                if(i!=dgv_results.RowCount)
                {
                    row.Cells[5].Value = "checked";
                }
              
            }
          
        }
    }
}
         