﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace OAMS_Interface_VFS
{
    public partial class addResult : Form
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand sql_cmd;
        private SQLiteDataAdapter DB;
        private DataSet DS = new DataSet();
        private DataTable DT = new DataTable();
        private SQLiteDataReader sql_reader;
        string SpecimenID = "";

        public addResult(string SID)
        {
            InitializeComponent();
            SpecimenID=SID;
            showResults();
            fillComboBox();


            tb_SID.Text = SID;
        }

        private void tb_TID_TextChanged(object sender, EventArgs e)
        {

        }
        public void fillComboBox()
        {
            String getMTs = "Select* from tbl_analytes";
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = getMTs;
            sql_reader = sql_cmd.ExecuteReader();
            while (sql_reader.Read())
            {
                cb_testList.Items.Add(sql_reader["TestCode"].ToString() + "|" + sql_reader["TestName"].ToString());
            }
            sql_con.Close();
        }


        private void btn_back_Click(object sender, EventArgs e)
        {
            Form frm_bound = new Bound(tb_SID.Text);
            frm_bound.Show();
            this.Close();

        }
        private void SetConnection()
        {
            sql_con = new SQLiteConnection("Data Source=fusionData.db;Version=3;New=False;Compress=True;");
        }


        private void btn_addResult_Click(object sender, EventArgs e)
        {
            string seqNumber = "0";
            string TestCode = tb_testName.Text;
            string DMV = tb_result.Text;
            string Unit = tb_unit.Text;
            string ResultFlags = "";
            string ResultStats = "";
            string OpID = "";
            string SpecimenID = tb_SID.Text;
            string saveResultFilter = "INSERT INTO tbl_results_order VALUES(null,'" + seqNumber.ToString() + "','" + TestCode.ToString() + "','" + DMV.ToString() + "','" + Unit.ToString() + "','" + ResultFlags.ToString() + "','" + ResultStats.ToString() + "','" + OpID.ToString() + "',date(),date(),'" + SpecimenID + "',null,null)";
            ExecuteQuery(saveResultFilter);
            showResults();

        }
        private void showResults()
        {
            dgv_results.Columns.Clear();
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            string CommandText = "Select resultNo,uniTestID  As 'Test',DMV As 'Result',Unit,SIRange As 'SI Range',ConvRange As 'Conv Range' from tbl_results_order WHERE SpecimenID='" + SpecimenID.ToString() + "' AND uniTestID !=''";
            DB = new SQLiteDataAdapter(CommandText, sql_con);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            dgv_results.DataSource = DT;
            sql_con.Close();


            DataGridViewButtonColumn btn_delete = new DataGridViewButtonColumn();
            btn_delete.HeaderText = "Delete Record";
            btn_delete.Text = "Delete";
            btn_delete.Name = "btn_delete";
            btn_delete.UseColumnTextForButtonValue = true;
            dgv_results.Columns.Add(btn_delete);

            

        }
        private void ExecuteQuery(string txtQuery)
        {
            SetConnection();
            sql_con.Open();
            sql_cmd = sql_con.CreateCommand();
            sql_cmd.CommandText = txtQuery;
            sql_cmd.ExecuteNonQuery();
            sql_con.Close();

        }

        private void addResult_Load(object sender, EventArgs e)
        {

        }

        private void cb_testList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string testraw = cb_testList.Text;
            string[] testarr = testraw.Split('|');
            tb_testID.Text = testarr[0].ToString();
            tb_testName.Text = testarr[1].ToString();

        }

        private void dgv_results_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string resultID = dgv_results.CurrentRow.Cells[0].Value.ToString();
            string removeSql = "Delete from tbl_results_order WHERE resultNo='" + resultID.ToString() + "'";
            ExecuteQuery(removeSql);
            showResults();
            
        }
    }
}
